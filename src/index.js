import React , { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Keyboard, Image } from 'react-native';
import { Container, Header, Content, Input, Row, Col, Item } from 'native-base';
import Chat from './Components/chat';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { getMessage } from './services';


class App extends Component {

    state = {
        msgText : '',
        chatMsg: []
    }


    componentDidMount(){

    }


    // onSend = () => {
    //     const { msgText, chatMsg }  = this.state;
    //     const len = chatMsg.length;
    //     if(msgText == "Hello"){
    //         var obj = {
    //             senderId : 122,
    //             text: msgText
    //         }
    //         var obj1 = {
    //             "recipient_id": "122",
    //             "text": "Hello. Let me Introduce Myself: I am Laya. I’ll be  your DBT Coach.",
    //         }
    //         const prev = (chatMsg.length > 0 && Object.keys(this.state.chatMsg[len - 1]) > 1)?this.state.chatMsg.slice(0, this.state.chatMsg.length - 1): this.state.chatMsg;
    //         this.setState({ chatMsg : (prev.length > 0)? [...prev, obj] : [obj, obj1] })
    //         this.setState({ msgText : ''})
    //         Keyboard.dismiss()
    //     } else if(msgText == "Image"){
    //         const obj = {
    //             "recipient_id": "122",
    //             "text": "Hello. Let me Introduce Myself: I am Laya. I’ll be  your DBT Coach.",
    //             "images":[
    //                 {
    //                     "recipient_id": "122",
    //                     "image": "https://i.imgur.com/nGF1K8f.jpg"
    //                 },
    //                 {
    //                 "recipient_id": "122",
    //                 "text": "Here is something to cheer you up:"
    //                 },
    //                 {
    //                     "recipient_id": "122",
    //                     "image": "https://i.imgur.com/nGF1K8f.jpg"
    //                 }
    //             ]
    //         }
    //         const prev = (Object.keys(this.state.chatMsg[len - 1]) > 1)?this.state.chatMsg.slice(0, this.state.chatMsg.length - 1): this.state.chatMsg;
    //         this.setState({ chatMsg : [...prev, obj] })
    //         this.setState({ msgText : ''})
    //         Keyboard.dismiss()
    //     } else if(msgText == "Video"){
    //         const obj = {}
    //     } else if(msgText == "Again"){
    //         const obj = {
    //                 "recipient_id": "122",
    //                 "text": "Hello. Let me Introduce Myself: I am Laya. I’ll be  your DBT Coach.",
    //                 // "buttons": [
    //                 //     {
    //                 //         "title": "Hola",
    //                 //         "payload": "hola"
    //                 //     },
    //                 //     {
    //                 //         "title": "Aloha",
    //                 //         "payload": "aloha"
    //                 //     },
    //                 //     {
    //                 //         "title": "Hello",
    //                 //         "payload": "hello"
    //                 //     }
    //                 // ]
    //         }
    //         const prev = (chatMsg.length > 0 && Object.keys(this.state.chatMsg[len-1]) > 1)?this.state.chatMsg.slice(0, this.state.chatMsg.length - 1): this.state.chatMsg;
    //         this.setState({ chatMsg :(prev.length > 0) ? [...prev, obj] : [] })
    //         this.setState({ msgText : ''})
    //         Keyboard.dismiss()
    //     }
    // }

    onSendClick = async () => {
        const { msgText, chatMsg }  = this.state;
        const len = chatMsg.length;
        var obj = {
            senderId : 120,
            text: msgText
        }
        const  {data}  =  await getMessage({ msgText })
        console.log("api call", data)
        if(data.length > 0){

            const prev = (chatMsg.length > 0 && Object.keys(this.state.chatMsg[len - 1]) > 1)?this.state.chatMsg.slice(0, this.state.chatMsg.length - 1): this.state.chatMsg;
            this.setState({ chatMsg : (prev.length > 0) ? [...prev, obj, ...data] : [obj, ...data] })
            this.setState({ msgText : ''})
            Keyboard.dismiss()
        } else {
            var obj = {
                senderId : 120,
                text: msgText
            }
            var obj1 = {
                recipient_id : 120,
                text: "Sorry, i didn't get you"
            }
            this.setState({ chatMsg : [...this.state.chatMsg, obj, obj1]})
            this.setState({ msgText : ''})
            Keyboard.dismiss()  
        }
    }
    

    onButtonClick = (selectedText, val) => {
        console.log(val)
        const bot = {
            "recipient_id": val.recipient_id,
            "text":val.text
        }
        const obj = {
            "senderId":120,
            "text":selectedText.title,
        }
        const prev = this.state.chatMsg.slice(0, this.state.chatMsg.length - 1)
        this.setState({
            chatMsg : [...prev, bot, obj]
        })
    }


    render(){
        return(
            <Container style={{flex:1}}>
                <Header style={{backgroundColor:'#2712a1', justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontSize:22, color:'white'}}>ChatBot</Text>
                </Header>
                <Content contentContainerStyle={{flex:1}} >
                    <ScrollView 
                        ref={ref => {this.scrollView = ref}}
                        onContentSizeChange={() => this.scrollView.scrollToEnd({animated: true})}
                        style={{flex:4, backgroundColor:'#efefef'}}>
                        {(this.state.chatMsg.length > 0) && this.state.chatMsg.map(e => {
                            return <Chat data={e} onButton={this.onButtonClick.bind(this)}/>
                        }) 
                        }
                    </ScrollView>
                    <Row style={{height:80,backgroundColor:'white', elevation:4}}>
                        <Col style={{flex:3, justifyContent:'center'}}>
                        <Item rounded>
                            <Input 
                                placeholder='Enter message' 
                                value={this.state.msgText}
                                onChangeText={(text) => { this.setState({ msgText: text }) }}
                                numberOfLines={4} 
                                />
                        </Item>
                        </Col>
                        <Col style={{flex:0.7, justifyContent:'center'}}>
                            <TouchableOpacity 
                                onPress={() => { this.onSendClick() }}
                                style={{justifyContent:'center', alignItems:'center'}}>
                                    <Image source={require('../assets/right-arrow.png')} style={{ width:25, height:25}}/>
                            </TouchableOpacity>
                        </Col>
                    </Row>
                </Content>
            </Container>
        )
    }
}


export default App;