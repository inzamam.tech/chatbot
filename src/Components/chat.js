import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Animated, Image, Dimensions } from 'react-native';
import { Content, Col, Row } from 'native-base';
import * as Animatable from 'react-native-animatable';
const {width, height} =  Dimensions.get('window')


class Chat extends Component {

    state = {
        slideUpValue: new Animated.Value(0),
    }

    componentDidMount(){
        return Animated.parallel([
            Animated.timing(this.state.slideUpValue, {
              toValue: 1,
              duration: 1000,
              useNativeDriver: true
            })
          ]).start();
    }


    onPressButton = (selectedText, obj) => {
        console.log("Button clicked")
        this.props.onButton(selectedText,obj)
    }

    onPressImageButton = () => {
        console.log("Image Button clicked")
    }

    render(){
        console.log("chat data", this.props.data)
        const { data } =  this.props;
        return (
            <>
            <View style={{flexDirection:'row',alignItems:'center',justifyContent:(data.senderId)?'flex-end':'flex-start',height:'auto', marginHorizontal:10, marginVertical:8}}>
                {(data.text) && (
                <Text style={{backgroundColor:(data.senderId)?'#ee68ee':'#6047eb',color:'white',paddingHorizontal:12, paddingVertical:6, fontSize:16, borderRadius:12}}>{data.text}</Text>
                )}
                {(data.image) && (
                    <Image source={{ uri: data.image}} style={{ width:120, height:120}}/>
                )}
            </View>
            {(data.buttons) && (
                <Row style={{flex:1, flexWrap:'wrap', justifyContent:'center'}}>
                    {(data.buttons.map(ele => {
                        return (
                            <Animated.View
                            style={{
                            transform: [
                                {
                                translateX: this.state.slideUpValue.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [400, 0]
                                })
                                }
                            ],
                            flex: 0,
                            flexWrap:'wrap',
                            width: 150,
                            borderRadius: 10,
                            margin:8,
                            backgroundColor: "#ee68ee",
                            justifyContent: "center",
                            alignItems:'center'
                            }}
                        >
                            <TouchableOpacity 
                                style={{width:'100%', justifyContent:'center', alignItems:'center'}}
                                onPress={() => { this.onPressButton(ele,data) }}
                                >
                                <Text style={{padding:10, color:'white', fontSize:17}}>{ele.title}</Text>
                            </TouchableOpacity>
                        </Animated.View>
                        )
                    }))}
                </Row>
            )}
            {(data.images) && (
                <Row style={{flex:1, flexWrap:'wrap'}}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    {(data.images.map(ele => {
                        return (
                            <Animated.View
                            style={{
                            transform: [
                                {
                                translateX: this.state.slideUpValue.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [400, 0]
                                })
                                }
                            ],
                            flex: 0,
                            flexWrap:'wrap',
                            width: 140,
                            height:140,
                            borderRadius: 10,
                            margin:8,
                            backgroundColor: "#6047eb",
                            justifyContent: "center",
                            alignItems:'center'
                            }}
                        >
                        <TouchableOpacity 
                            onPress={() => { this.onPressImageButton() }}
                            style={{width:'100%', height:'100%', elevation:3, backgroundColor:'white', justifyContent:'center', alignItems:'center'}}>
                                {(ele.image) ? (
                                <Image source={{ uri: `${ele.image}`}} resizeMode={'stretch'} style={{ width:'100%', height:'100%'}}/>
                                ):
                                <Text style={{alignSelf:'center', textAlign:'center', paddingHorizontal:4}}>{ele.text}</Text>
                                }
                        </TouchableOpacity>
                        </Animated.View>
                        )
                    }))}
                    </ScrollView>
                </Row>
            )}
            </>
        )
    }
}


export default Chat;







                {/* <Row style={{flex:1, flexWrap:'wrap'}}>
                    <TouchableOpacity 
                        onPress={() => { this.onPressButton() }}
                        style={{width:150, elevation:3, margin:10, backgroundColor:'yellow', borderRadius:15}}>
                        <Text style={{padding:10}}>One</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={() => { this.onPressButton() }}
                        style={{width:150, elevation:3, margin:10, backgroundColor:'gray', borderRadius:15}}>
                        <Text style={{padding:10}}>One</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:150, elevation:3 ,margin:10, backgroundColor:'green', borderRadius:15}}>
                        <Text style={{padding:10}}>One</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:150, elevation:3, margin:10, backgroundColor:'yellow', borderRadius: 15}}>
                        <Text style={{padding:10}}>One</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:150, elevation:3, margin:10, backgroundColor:'yellow', borderRadius: 15}}>
                        <Text style={{padding:10}}>One</Text>
                    </TouchableOpacity>
                </Row>
                <Row>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <TouchableOpacity 
                        onPress={() => { this.onPressImageButton() }}
                        style={{width:80, height:80, backgroundColor:'gray', margin:8}}>
                        </TouchableOpacity>
                        <TouchableOpacity 
                        onPress={() => { this.onPressImageButton() }}
                        style={{width:80, height:80, backgroundColor:'gray', margin:8}}>
                        </TouchableOpacity>
                        <TouchableOpacity 
                        onPress={() => { this.onPressImageButton() }}
                        style={{width:80, height:80, backgroundColor:'gray', margin:8}}>
                        </TouchableOpacity>
                        <TouchableOpacity 
                        onPress={() => { this.onPressImageButton() }}
                        style={{width:80, height:80, backgroundColor:'gray', margin:8}}>
                        </TouchableOpacity>
                    </ScrollView>
                </Row> */}